FROM python:3.8-slim-buster
ADD ./hwserver.py /home/hwserver.py
WORKDIR /home
RUN pip install flask
EXPOSE 5000
CMD [ "python", "./hwserver.py"]